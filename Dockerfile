FROM golang:alpine

ADD ./src /go/src/app

WORKDIR /go/src/app

#RUN sed -i 's/archive.ubuntu.com/mirrors.rit.edu/' /etc/apt/sources.list
RUN apk update
RUN apk upgrade
RUN apk add git make openssh-client

RUN go get golang.org/x/oauth2
RUN go get -u \
  github.com/codegangsta/gin \
  github.com/go-sql-driver/mysql \
  github.com/gorilla/mux \
  github.com/gorilla/sessions \
  golang.org/x/crypto/bcrypt \
  github.com/kataras/go-mailer \
  cloud.google.com/go/compute/metadata

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["app"]
