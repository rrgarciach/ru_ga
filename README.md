# Golang Test App

This is a Test Application

## Getting Started

### Prerequisites

- Install Docker and/or Docker-compose on system.

#### Running with Docker Compose

- Clone this repo on local machine and navigate into directory.
- Build and run application: `docker-compose up`
- App will be served at localhost:3030

### Testing

`go test`

### Deploy:

To deploy on Heroku's instance simply push/build and release:

`heroku container:push web --app <app-name>`

`heroku container:release web --app <app-name>`

### Additional hints

* To get access to container's internal bash as root user terminal simply run (local development only):

`docker exec -u root -it golang-test-app sh`

## Built With

* [Docker](https://docs.docker.com/compose/install) - Used to build and run application container and its services
* [Go](https://golang.org) - Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://git-codecommit.us-east-1.amazonaws.com/v1/repos/sos-api/tags).
