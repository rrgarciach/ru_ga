CREATE DATABASE `golang-test`;
USE `golang-test`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(50) DEFAULT '',
  `address` varchar(100) DEFAULT '',
  `telephone` varchar(20) DEFAULT '',
  `strategy` varchar(20) DEFAULT 'local',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
);
