package main

import (
  "log"
  "strings"
  "net/http"
  "encoding/json"
  "database/sql"

  _ "github.com/go-sql-driver/mysql"

  "app/models"
)

// Handle Local Auth requests
func (a *App) authenticateHandler(w http.ResponseWriter, r *http.Request) {
  var u models.User
  log.Println("Request received:", r.Method, r.URL)

  decoder := json.NewDecoder(r.Body) // Get credentials from payload
  if err := decoder.Decode(&u); err != nil {
    respondWithError(w, http.StatusBadRequest, "Invalid parameters")
    return
  }
  defer r.Body.Close()

  u, err := a.UserHelper.LocalAuth(u) // Pass credentials to User Helper to check them
  if err != nil {
    switch err {
    case sql.ErrNoRows: // If User was not found in data store
      respondWithError(w, http.StatusUnauthorized, "Unauthorized")  // status 401
    default:
      if strings.Contains(err.Error(), "Not match") {
        respondWithError(w, http.StatusUnauthorized, "Unauthorized") // status 401
        } else {
          log.Println(err.Error())
          respondWithError(w, http.StatusInternalServerError, "Unhandled exception") // status 500
        }
      }
      return
  }

  a.createSession(u.Id, w, r)
  u.Password = "" // never return password
  respondWithJSON(w, http.StatusOK, u)
}

// Handle edit profile request
func (a *App) editMeHandler(w http.ResponseWriter, r *http.Request) {
  var u models.User
  log.Println("Request received:", r.Method, r.URL)

  // Check if session is flagged as authenticated
  if !isAuthenticated(r) {
    respondWithError(w, http.StatusUnauthorized, "Unauthorized")
    return
  }

  decoder := json.NewDecoder(r.Body) // Get data from payload
  if err := decoder.Decode(&u); err != nil {
    respondWithError(w, http.StatusBadRequest, "Invalid parameters")
    return
  }
  defer r.Body.Close()

  su, err := a.loadSessionUser(store, r) // load session's User
  if err != nil {
    respondWithError(w, http.StatusUnauthorized, "Unauthorized")
    return
  }

  u.Id = su.Id // pass session's User ID as it's editing itself
  u, err = a.UserHelper.Update(u) // update User
  if err != nil {
    switch err {
    case sql.ErrNoRows:
      respondWithError(w, http.StatusNotFound, "User not found") // status 404
    default:
      if strings.Contains(err.Error(), "Error 1064") {
        respondWithError(w, http.StatusBadRequest, "Invalid parameters") // status 400
      } else if strings.Contains(err.Error(), "Error 1062") {
        respondWithError(w, http.StatusConflict, "User already registered") // status 409
      } else {
        log.Println(err.Error())
        respondWithError(w, http.StatusInternalServerError, "Unhandled exception") // status 500
      }
    }
    return
  }
  u.Password = "" // never return password
  respondWithJSON(w, http.StatusOK, u)
}

// Handle reset password request
func (a *App) resetPasswordHandler(w http.ResponseWriter, r *http.Request) {
  var u models.User
  log.Println("Request received:", r.Method, r.URL)

  decoder := json.NewDecoder(r.Body) // Get credentials from payload
  if err := decoder.Decode(&u); err != nil {
    respondWithError(w, http.StatusBadRequest, "Invalid parameters") // status 400
    return
  }
  defer r.Body.Close()

  u, err := a.UserHelper.GetByEmail(u.Email) // load email's User
  if err != nil {
    switch err {
    case sql.ErrNoRows: // If User was not found in data store
      respondWithError(w, http.StatusNotFound, "Not found") // status 404
    default:
      log.Println(err.Error())
      respondWithError(w, http.StatusInternalServerError, "Unhandled exception") // status 500
      return
    }
  }

  // Change password with new random value:
  newPassword, err := a.UserHelper.ResetPassword(u)
  if err != nil {
    switch err {
    case sql.ErrNoRows:
      respondWithError(w, http.StatusNotFound, "Not found")
    default:
      log.Println(err.Error())
      respondWithError(w, http.StatusInternalServerError, "Unhandled exception")
      return
    }
  }

  // Send email with new random password:
  sendResetPasswordEmail(u.Email, newPassword)
  respondWithJSON(w, http.StatusOK, nil)
}

// Handle local sign up request
func (a *App) signUpHandler(w http.ResponseWriter, r *http.Request) {
  var u models.User
  log.Println("Request received:", r.Method, r.URL)

  decoder := json.NewDecoder(r.Body) // load email and password from payload
  if err := decoder.Decode(&u); err != nil {
    respondWithError(w, http.StatusBadRequest, "Invalid parameters")
    return
  }
  defer r.Body.Close()

  u, err := a.UserHelper.CreateLocal(u) // create new User with local auth strategy
  if err != nil {
    switch err {
    case sql.ErrNoRows: // If User was not found in data store
      respondWithError(w, http.StatusNotFound, "User not found")
    default:
      if strings.Contains(err.Error(), "Error 1064") {
        respondWithError(w, http.StatusBadRequest, "Invalid parameters") // status 400
      } else if strings.Contains(err.Error(), "Error 1062") {
        respondWithError(w, http.StatusConflict, "User already registered") // status 509
      } else {
        log.Println(err.Error())
        respondWithError(w, http.StatusInternalServerError, "Unhandled exception") // status 500
      }
    }
    return
  }

  u.Password = "" // never return password
  a.createSession(u.Id, w, r)
  respondWithJSON(w, http.StatusCreated, u)
}

// Handle OAuth  authentication request
func (a *App) oAuth2Handler(w http.ResponseWriter, r *http.Request) {
  log.Println("Request received:", r.Method, r.URL)

  // handle parameters from callback
  u, err := handleGoogleCallback(w, r)
  if err != nil {
    log.Println(err.Error())
    respondWithError(w, http.StatusBadRequest, "Unhandled exception")
    return
  }

  eu, err := a.UserHelper.GetByEmail(u.Email) // load email's User
  if err != nil {
    switch err {
      case sql.ErrNoRows: // If User is not registered
      u, err := a.UserHelper.CreateOAuth(u)
      if err != nil {
        log.Println(err.Error())
        respondWithError(w, http.StatusInternalServerError, "Unhandled exception")
      }
      session, err := store.Get(r, "golang-test-app")
      if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
      }
      session.Values["authenticated"] = true
      session.Values["user_id"] = u.Id
      session.Save(r, w)
      u.Password = "" // never return password
      http.Redirect(w, r, "/", http.StatusTemporaryRedirect)

    default:
      // Unhandler errors
      log.Println(err.Error())
      respondWithError(w, http.StatusInternalServerError, "Unhandled exception")
      return
    }
  }

  eu.Password = "" // never return password
  a.createSession(eu.Id, w, r)
  http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}
