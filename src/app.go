package main

import (
  "fmt"
  "log"
  "net/http"
  "encoding/json"
  "database/sql"

  "github.com/gorilla/mux"
  _ "github.com/go-sql-driver/mysql"

  "app/models"
)

type App struct {
  Router      *mux.Router
  DB          *sql.DB
  UserHelper  models.UserHelper
}

// Initializes Application by getting Helper and Router
func (a *App) Initialize(user, password, dbname, host string) {
  log.Println("Connecting to database...")
  connectionStr := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", user, password, host, dbname)

  var err error
  a.DB, err = sql.Open("mysql", connectionStr)
  if err != nil {
    log.Fatal(err)
  }

  a.UserHelper = models.UserHelper{DB: a.DB}

  a.Router = mux.NewRouter()
  log.Println("App initialized")
}

// Runs application and start listening
func (a *App) Run(port string) {
  a.initializeRoutes()
  log.Fatal(http.ListenAndServe(":" + port, a.Router))
  log.Println("Running on port " + port)
}

// Sends REST API error message
func respondWithError(w http.ResponseWriter, code int, message string) {
    respondWithJSON(w, code, map[string]string{"error": message})
}

// Sends REST API success message with payload
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
    response, _ := json.Marshal(payload)
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(code)
    w.Write(response)
}
