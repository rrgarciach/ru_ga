function goToOAuth2Login(e) {
  e.preventDefault();
  window.location.href = '/login/oauth2'
}

function submitSignUp(e) {
  e.preventDefault();
  const email = document.getElementById('email').value;
  const password = document.getElementById('password').value;
  const password2 = document.getElementById('password2').value;
  if (password !== password2) {
    alert('Password does not match')
    return
  }
  fetch('/api/auth/signUp', {
    body: JSON.stringify({email, password}),
    method: 'POST',
    credentials: 'include',
  })
  .then(({status}) => {
    if (status === 201) {
      window.location.href = '/register';
    } else {
      alert('E-mail already registered');
    }
  });
}

function submitRegister(e) {
  e.preventDefault();
  const fullName = document.getElementById('full_name').value;
  const email = document.getElementById('email').value;
  const address = document.getElementById('autocomplete').value;
  const telephone = document.getElementById('telephone').value;
  fetch('/api/me', {
    body: JSON.stringify({fullName, email, address, telephone}),
    method: 'PUT',
    credentials: 'include',
  })
  .then(({status}) => {
    if (status === 200) {
      window.location.href = '/profile';
    } else {
      alert('There was an error');
    }
  });
}

function submitLogin(e) {
  e.preventDefault();
  const email = document.getElementById('email').value;
  const password = document.getElementById('password').value;
  fetch('/api/auth', {
    body: JSON.stringify({email, password}),
    method: 'POST',
    credentials: 'include',
  })
  .then(({status}) => {
    if (status === 200) {
      window.location.href = '/profile';
    } else {
      alert('Invalid credentials');
    }
  });
}

function signOut(e) {
  e.preventDefault();
  window.location.href = '/logout';
}

function goToEditMode(e) {
  e.preventDefault();
  window.location.href = '/profile/edit'
}

function submitEdit(e) {
  e.preventDefault();
  const fullName = document.getElementById('full_name').value;
  const email = document.getElementById('email').value;
  const address = document.getElementById('autocomplete').value;
  const telephone = document.getElementById('telephone').value;
  fetch('/api/me', {
    body: JSON.stringify({fullName, email, address, telephone}),
    method: 'PUT',
    credentials: 'include',
  })
  .then(({status}) => {
    if (status === 200) {
      window.location.href = '/profile';
    } else {
      alert('There was an error');
    }
  });
}

function submitResetPassword(e) {
  e.preventDefault();
  const email = document.getElementById('email').value;
  fetch('/api/me/resetPassword', {
    body: JSON.stringify({email}),
    method: 'POST',
    credentials: 'include',
  })
  .then(({status}) => {
    if (status === 200) {
      alert('Please check your e-mail to find you new password');
      window.location.href = '/';
    } else if (status === 404) {
      alert(`There's no account matching with provided email`);
    } else {
      alert('There was an error');
    }
  });
}
