package main

import (
  "log"
  "net/http"
  "errors"
  "database/sql"

  "github.com/gorilla/sessions"

  "app/models"
)

// Loads user from session
func (a *App) loadSessionUser (store sessions.Store, r *http.Request) (models.User, error) {
  // Load session from cookies
  session, err := store.Get(r, "golang-test-app")
  if err != nil {
    log.Println(err.Error())
    return models.User{}, err
  }

  // Check if there's no User ID attached
  if session.Values["user_id"] == nil {
    log.Println("Not User ID provided in session or not session provided")
    return models.User{}, errors.New("No User ID")
  }

  id := session.Values["user_id"].(int) // Extract User ID from Cookie
  u, err := a.UserHelper.GetById(id) // Get User's data
  if err != nil {
    switch err {
    case sql.ErrNoRows: // If User was not found in data store
      return u, err
    default:
      log.Println("error")
    }
    return u, err
  }
  return u, err
}

// Creates new session
func (a *App) createSession(userId int, w http.ResponseWriter, r *http.Request) {
  // Load session from cookies
  session, err := store.Get(r, "golang-test-app")
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  // Set session values
  session.Values["authenticated"] = true
  session.Values["user_id"] = userId
  session.Save(r, w)
}

// Creates new session
func (a *App) closeSession(w http.ResponseWriter, r *http.Request) {
  // Load session from cookies
  session, err := store.Get(r, "golang-test-app")
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  // Clean session
	session.Values["authenticated"] = false // set flag as NOT authenticated
  session.Values["user_id"] = 0 // clean User ID
	session.Save(r, w) // Save session changes
}

// Middleware to check if cookie session was provided in request
func isAuthenticated(r *http.Request) bool {
  // Check if cookie contains session
  session, _ := store.Get(r, "golang-test-app")
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		return false
	}
  return true
}
