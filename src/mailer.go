package main

import (
  "github.com/kataras/go-mailer"
)

// sender configuration.
var (
  config = mailer.Config{
    Host:     getEnv("MAILGUN_SMTP_SERVER", "smtp.mailgun.org"),
    Username: getEnv("MAILGUN_SMTP_LOGIN", ""),
    Password: getEnv("MAILGUN_PASSWORD", ""),
    FromAddr: getEnv("MAILGUN_SMTP_LOGIN", ""),
    Port:     587,
    UseCommand: false,
  }
)

// Sets up Mailer service and sends reset password email
func sendResetPasswordEmail(email, password string) {
  // initalize a new mail sender service.
  sender := mailer.New(config)
  // the subject/title of the e-mail.
  subject := "Password Reset - Golang Test App"
  // the rich message body.
  content := `<h1>Your new password is:</h1> <br/> <span>` + password + `</span>`
  // the recipient(s).
  to := []string{email}
  // send the e-mail.
  err := sender.Send(subject, content, to...)
  if err != nil {
    println("error while sending the e-mail: " + err.Error())
  }
}
