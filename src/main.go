package main

import (
	"os"
)

func main() {
	// Load DB parameters from environment variables
	PORT := getEnv("PORT", "3001")
	MYSQL_USER := getEnv("MYSQL_USER", "root")
	MYSQL_PASSWORD := getEnv("MYSQL_PASSWORD", "")
	MYSQL_DATABASE := getEnv("MYSQL_DATABASE", "golang-test")
	MYSQL_HOST := getEnv("MYSQL_HOST", "localhost")

	// Initialize Application
	a := App{}
	a.Initialize(MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_HOST)

	a.Run(PORT) // Run application and start listening
}

// Function to load ENV variables or provide default value if not found
func getEnv(key, defaultValue string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return defaultValue
    }
    return value
}
