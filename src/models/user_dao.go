package models

import (
  "log"
  "fmt"
  "errors"
  "database/sql"
  "math/rand"

  "golang.org/x/crypto/bcrypt"
)

type User struct {
  Id        int    `json:"id,omitempty"`
  Email     string `json:"email,omitempty"`
  Password  string `json:"password,omitempty"`
  FullName  string `json:"fullName,omitempty"`
  Address   string `json:"address,omitempty"`
  Telephone string `json:"telephone,omitempty"`
  Stratgy   string `json:"strategy,omitempty"`
}

func (u *User) GetUserById(db *sql.DB) error {
  return db.QueryRow("SELECT email, password, full_name, address, telephone FROM users WHERE id=?", u.Id).Scan(&u.Email, &u.Password, &u.FullName, &u.Address, &u.Telephone)
}

func (u *User) GetByEmail(db *sql.DB) error {
  return db.QueryRow("SELECT id, password, full_name, address, telephone FROM users WHERE email=?", u.Email).Scan(&u.Id, &u.Password, &u.FullName, &u.Address, &u.Telephone)
}

func (u *User) CreateUserLocal(db *sql.DB) error {
  hashedPassword, err := hashPassword(u.Password);
  if err != nil {
    return err
  }
  _, err = db.Query("INSERT INTO users (email, password, strategy) VALUES (?, ?, 'local')", u.Email, hashedPassword)
  if err != nil {
    return err
  }
  err = db.QueryRow("SELECT id FROM users WHERE email=?", u.Email).Scan(&u.Id)
  if err != nil {
    return err
  }
  return nil
}

func (u *User) UpdateUser(db *sql.DB) error {
  stmt := fmt.Sprintf("UPDATE users SET email='%s', full_name='%s', address='%s', telephone='%s' WHERE id=%d", u.Email, u.FullName, u.Address, u.Telephone, u.Id)
  _, err := db.Exec(stmt)
  if err != nil {
    log.Println(err.Error())
    return err
  }
  return nil
}

func (u *User) LocalAuthenticate(db *sql.DB) error {
  password := u.Password
  err := db.QueryRow("SELECT id, password FROM users WHERE strategy='local' AND email=?", u.Email).Scan(&u.Id, &u.Password)
  if err != nil {
    return err
  }

  match := checkPasswordHash(password, u.Password)
  if match != true {
    return errors.New("Not match")
  }
  return nil
}

func (u *User) CreateUserOAuth(db *sql.DB) error {
  _, err := db.Query("INSERT INTO users (email, password, full_name, strategy) VALUES (?, ?, ?, 'google')", u.Email, u.Id, u.FullName)
  if err != nil {
    return err
  }
  err = db.QueryRow("SELECT id FROM users WHERE email=?", u.Email).Scan(&u.Id)
  if err != nil {
    return err
  }
  return nil
}

func (u *User) OAuthenticate(db *sql.DB) error {
  err := db.QueryRow("SELECT id, password, strategy FROM users WHERE strategy='google' AND email=?", u.Email).Scan(&u.Id)
  if err != nil {
    return err
  }
  return nil
}

func (u *User) ResetPassword(db *sql.DB) (string, error) {
  password := GenerateRandomPassword(20)
  hashedPassword, err := hashPassword(password);
  if err != nil {
    return "", err
  }
  stmt := fmt.Sprintf("UPDATE users SET password='%s' WHERE id=%d", hashedPassword, u.Id)
  _, err = db.Exec(stmt)
  if err != nil {
    log.Println(err.Error())
    return "", err
  }
  return password, nil
}

func GenerateRandomPassword(n int) string {
    var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

    b := make([]rune, n)
    for i := range b {
        b[i] = letter[rand.Intn(len(letter))]
    }
    return string(b)
}

func hashPassword(password string) (string, error) {
  bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
  return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
  err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
  return err == nil
}
