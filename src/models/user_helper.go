package models

import (
  "database/sql"
)

type UserHelper struct {
  DB    *sql.DB
}

/*
The purpose of this helper is to reduce coupling between models and API handlers
*/
func (h *UserHelper) GetById(id int) (User, error) {
  u := User{Id: id}
  return u, u.GetUserById(h.DB)
}

func (h *UserHelper) GetByEmail(email string) (User, error) {
  u := User{Email: email}
  return u, u.GetByEmail(h.DB)
}

func (h *UserHelper) CreateLocal(u User) (User, error) {
  return u, u.CreateUserLocal(h.DB)
}

func (h *UserHelper) Update(u User) (User, error) {
  return u, u.UpdateUser(h.DB)
}

func (h *UserHelper) LocalAuth(u User) (User, error) {
  return u, u.LocalAuthenticate(h.DB)
}

func (h *UserHelper) CreateOAuth(u User) (User, error) {
  return u, u.CreateUserOAuth(h.DB)
}

func (h *UserHelper) OAuth(u User) (User, error) {
  return u, u.OAuthenticate(h.DB)
}

func (h *UserHelper) ResetPassword(u User) (string, error) {
  return u.ResetPassword(h.DB)
}
