package main

import (
  "log"
  "net/http"
  "errors"
  "encoding/json"

  "golang.org/x/oauth2"
  "golang.org/x/oauth2/google"

  "app/models"
)

type GoogleProfile struct {
  Id     string
  Email     string
  Name      string
}

// Declare OAuth config
var (
  googleOauthConfig = &oauth2.Config{
    ClientID: getEnv("OAUTH_CLIENT_ID", ""),
    ClientSecret: getEnv("OAUTH_CLIENT_SECRET", ""),
    RedirectURL: getEnv("OAUTH_REDIRECT_URL", ""),
    Scopes: []string{
      "https://www.googleapis.com/auth/userinfo.email",
    },
    Endpoint: google.Endpoint,
  }
  oauthStateString = "random"
)

// Handle incoming requests for OAuth
func handleGoogleLogin(w http.ResponseWriter, r *http.Request) {
  // Generate Auth URL and redirect to it:
  url := googleOauthConfig.AuthCodeURL(oauthStateString)
  http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

// Handle OAuth Callback
func handleGoogleCallback(w http.ResponseWriter, r *http.Request) (models.User, error) {
  // Load state string from request and validate it
  state := r.FormValue("state")
  if state != oauthStateString {
    log.Printf("Invalid oauth state, expected '%s', got '%s'\n", oauthStateString, state)
    http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
    return models.User{}, errors.New("Authentication Error")
  }

  // Load code and generate token
  code := r.FormValue("code")
  token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)
  if err != nil {
    log.Println("Code exchange failed with '%s'\n", err)
    http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
    return models.User{}, errors.New("Authentication Error")
  }

  // Use token to retrieve Google Profile's data
  response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)

  // Hydrate Google Profile struct and validate
  gp := GoogleProfile{}
  decoder := json.NewDecoder(response.Body)
  if err := decoder.Decode(&gp); err != nil {
    log.Println("Format error: '%s'\n", err)
    http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
    return models.User{}, errors.New("Authentication Error")
  }

  // Return Google Profile parsed as User
  return models.User{Email: gp.Email, FullName: gp.Name, Password: gp.Id}, nil
}
