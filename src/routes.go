package main

import (
  "net/http"
)

func (a *App) initializeRoutes() {
  // RESTful API routes:
  a.Router.HandleFunc("/api/auth", a.authenticateHandler).Methods("POST")
  a.Router.HandleFunc("/api/auth/signUp", a.signUpHandler).Methods("POST")
  a.Router.HandleFunc("/api/me", a.editMeHandler).Methods("PUT")
  a.Router.HandleFunc("/api/me/resetPassword", a.resetPasswordHandler).Methods("POST")

  // Server side rendered UI routes:
  a.Router.HandleFunc("/", a.serveIndexHandler)
  a.Router.HandleFunc("/login", a.serveIndexHandler)
  a.Router.HandleFunc("/login/oauth2", handleGoogleLogin)
  a.Router.HandleFunc("/logout", a.serveLogoutHandler)
  a.Router.HandleFunc("/reset-password", a.serveResetPasswordHandler)
  a.Router.HandleFunc("/sign-up", a.serveSignUpHandler)
  a.Router.HandleFunc("/sign-up/oauth2", handleGoogleLogin)
  a.Router.HandleFunc("/callback", a.oAuth2Handler)
  a.Router.HandleFunc("/register", a.serveRegisterHandler)
  a.Router.HandleFunc("/profile", a.serveProfileHandler)
  a.Router.HandleFunc("/profile/edit", a.serveProfileEditHandler)

  // serve static assets at root path:
  a.Router.PathPrefix("/").Handler(http.FileServer(http.Dir("./assets/")))
}
