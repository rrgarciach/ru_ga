package main

import (
  "html/template"
  "net/http"

  "github.com/gorilla/sessions"
)

// Cookie session config
var (
  key = []byte(getEnv("SESSION_SECRET", "secret"))
  store = sessions.NewCookieStore(key) // Generate session store
)

// Serves Index and login views
func (a *App) serveIndexHandler(w http.ResponseWriter, r *http.Request) {
  redirectAuthenticated(w, r) // Redirect logged in users

  // Render view
  tmpl := template.Must(template.ParseFiles("templates/index.tmpl"))
  tmpl.Execute(w, nil)
}

// Serves logout view
func (a *App) serveLogoutHandler(w http.ResponseWriter, r *http.Request) {
  redirectGuest(w, r) // Redirect NOT logged in users
  a.closeSession(w, r)
  http.Redirect(w, r, "/", http.StatusSeeOther) // Redirect to index
}

// Serves reset password view
func (a *App) serveResetPasswordHandler(w http.ResponseWriter, r *http.Request) {
  redirectAuthenticated(w, r) // Redirect logged in users

  // Render view
  tmpl := template.Must(template.ParseFiles("templates/reset_form.tmpl"))
  tmpl.Execute(w, nil)
}

// Serves sign up view
func (a *App) serveSignUpHandler(w http.ResponseWriter, r *http.Request) {
  redirectAuthenticated(w, r) // Redirect logged in users

  // Render view
  tmpl := template.Must(template.ParseFiles("templates/signup.tmpl"))
  tmpl.Execute(w, nil)
}

// Serves register view
func (a *App) serveRegisterHandler(w http.ResponseWriter, r *http.Request) {
  redirectGuest(w, r) // Redirect NOT logged in users

  u, err := a.loadSessionUser(store, r) // load session's User
  if err != nil {
    a.closeSession(w, r)
    redirectGuest(w, r) // If User data is not found, reject access
    return
  }

  // Render view
  tmpl := template.Must(template.ParseFiles("templates/register.tmpl"))
  tmpl.Execute(w, u)
}

// Serves profile view
func (a *App) serveProfileHandler(w http.ResponseWriter, r *http.Request) {
  redirectGuest(w, r) // Redirect NOT logged in users

  // Validate session (check if User exists)
  u, err := a.loadSessionUser(store, r)
  if err != nil {
    a.closeSession(w, r)
    redirectGuest(w, r) // If User data is not found, reject access
    return
  }

  // Render view
  tmpl := template.Must(template.ParseFiles("templates/profile.tmpl"))
  tmpl.Execute(w, u)
}

// Serves profile edit view
func (a *App) serveProfileEditHandler(w http.ResponseWriter, r *http.Request) {
  redirectGuest(w, r)

  // Validate session (check if User exists)
  u, err := a.loadSessionUser(store, r)
  if err != nil {
    a.closeSession(w, r)
    redirectGuest(w, r) // If User data is not found, reject access
    return
  }

  // Render view
  tmpl := template.Must(template.ParseFiles("templates/profile_edit.tmpl"))
  tmpl.Execute(w, u)
}

// Middleware to redirect User from views that only public (like login view)
func redirectAuthenticated(w http.ResponseWriter, r *http.Request) {
  if isAuthenticated(r) {
    http.Redirect(w, r, "/profile", http.StatusSeeOther)
  }
}

// Middleware to redirect User without session from protected views
func redirectGuest(w http.ResponseWriter, r *http.Request) {
  if !isAuthenticated(r) {
    http.Redirect(w, r, "/login", http.StatusSeeOther)
  }
}
